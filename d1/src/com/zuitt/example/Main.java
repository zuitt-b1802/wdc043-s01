package com.zuitt.example;

import java.util.Scanner;

public class Main {
    /*
        Main Class

        The Main class is the entry point of our Java program. It is responsible for executing our code. Every Java program should have at least 1 class and 1 function inside it.
    */

    public static void main(String[] args){
        /*
            -main function is where most executable code is applied to.

            -You can create an intellij Java project with a template that already uses or has a main class and function

            - The "public" keyword is what we call an access modifier, it tells our application what parts of the program can access the main function.

            - The "void" is the return statement's data type of the "main" function that it defines what kind of data will be returned. "void" means that the main function will return nothing.
         */
        System.out.println("Hello World");

        //variables and data types
        // in Java, to be able to declare a variable, we have to identify or declare its data type. Which means, that the variable will expect and only accept data with the type declared.

        int myNum;
        myNum = 3;
        System.out.println(myNum);

        myNum = 50;
        System.out.println(myNum);

        //byte = -128 to 127
        byte students = 127;
        //short = -32,768 to 32,767
        short seats = 32767;
        System.out.println(students);
        System.out.println(seats);
        int localPopulation = 2147483647;
        long worldPopulation = 7862881145L;
        //L is added at the end of the long number to be recognized as long. If that is omitted, it will wrongfully recognized as int.
        System.out.println(localPopulation);
        System.out.println(worldPopulation);

        //float can only have 7 digits (incluedes number before decimal point) AND the final decimal place is being rounded off
        float piFloat = 3.1415921f;
        double piDouble = 3.1415921;
        System.out.println(piFloat);
        System.out.println(piDouble);

        char letter = 'c';
        System.out.println(letter);

        //boolean
        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        //final - creates a constant variable - sam as in JS, it cannot be re-assigned
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        //String - In Java, is non-primitive data type. Because in Java, Strings are objects that can use methods
        //Non-primitive data types have access to methods.
        //Array, Class, Interface

        String username = "chesterSmith29";
        System.out.println(username);
        System.out.println(username.isEmpty());

        username = "";
        System.out.println(username.isEmpty());

        //Scanner - is a class which allows us to input into our program, it's kind of similar to prompt() in JS
        //However, Scanner, being a class, has to be imported to be used.
        Scanner scannerName = new Scanner(System.in);
        String myName = scannerName.nextLine();
        System.out.println("Your name is " +myName+"!");

        int myAge = scannerName.nextInt();
        System.out.println("You are "+myAge+" years Old!");

        System.out.println("What is your first quarter grade?");
        double firstQuarter = scannerName.nextDouble();

        System.out.println("Your first quarter grade is "+firstQuarter+"!");
        System.out.println("Enter a number:");
        int number1 = scannerName.nextInt();
        System.out.println("Enter a number:");
        int number2 = scannerName.nextInt();

        int sum = number1 + number2;
        System.out.println("The sum of the numbers are: "+sum);
        System.out.println("ACTY ~~~~~~~~");
        System.out.println("Please enter a number1");
        int exerNum1 = scannerName.nextInt();
        System.out.println("Please enter a number2");
        int exerNum2 = scannerName.nextInt();
        int result = exerNum1 - exerNum2;
        System.out.println(result);
    }
}
