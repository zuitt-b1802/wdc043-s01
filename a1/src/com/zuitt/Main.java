package com.zuitt;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner scannerName = new Scanner(System.in);
        System.out.println("What is your first name?");
        String fName = scannerName.nextLine();
        System.out.println("What is your last name?");
        String lName = scannerName.nextLine();

        System.out.println("First Subject Grade?");
        double firstSub = scannerName.nextDouble();

        System.out.println("Second Subject Grade?");
        double secondSub = scannerName.nextDouble();

        System.out.println("Third Subject Grade?");
        double thirdSub = scannerName.nextDouble();

        System.out.println("Welcome"+" "+fName+" "+lName);
        double finalAve = (firstSub+secondSub+thirdSub)/3;
        System.out.println("This is the average of your grades: "+finalAve);
    }
}
